const { MongoClient } = require('mongodb');

const DATABASE_URL = 'mongodb+srv://<user>:<senha>@cluster.d47di.mongodb.net/chat-venda?retryWrites=true&w=majority';

const client = new MongoClient(DATABASE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

module.exports = async function connect() {
  if (!client.isConnected()) await client.connect();

  const db = client.db('chat-venda');
  return { db, client };
}
