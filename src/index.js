const connect = require('./database');

async function createMessage() {
  const { db } = await connect();
  const response = await db.collection('chat-anuncio').insertOne({
    idAnuciante: 5,
    nmAnunciante: "Jao",
    idAnuncio: 3,
    idComprador: 7,
    nmComprador: "Lucas",
    envioBy: 5,
    isLeitura: false,
    message: 'ok, vi aqui, voce esta interessado?',
    updateAt: new Date(),
  });

  console.log('response: ', response.ops);
}

async function allMessagesByAnucio(idAnuncio) {
  const { db } = await connect();
  const response = await db
    .collection('chat-anuncio')
    .find({ idAnuncio })
    .toArray();

  console.log('response: ', response);
}

async function messageByIdAnuciante(id) {
  const { db } = await connect();
  const response = await db
    .collection('chat-anuncio')
    .find({ idComprador: id })
    .toArray();

  console.log('response: ', response);
}

async function updateReadMessageById(envioBy) {
  const { db } = await connect();
  const response = await db
    .collection('chat-anuncio')
    .updateMany({ envioBy }, [
      { $set: { isLeitura: true, updateAt: new Date() } },
    ]);

  console.log('response: ', response);
}

allMessagesByAnucio(3);

// https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/
